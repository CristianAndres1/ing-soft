﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoloMastersWeb.Models
{
    public class MasterPost
    {
        [Key]
        public int masterPostId { get; set; }
        public string masterPostTitle { get; set; }
        public string masterPostDescription { get; set; }
        public string masterOwner { get; set; }
        public DateTime masterPostDate { get; set; }
        public string imgUrl { get; set; }
    }
}
