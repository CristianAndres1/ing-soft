﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoloMastersWeb.Models
{
    public class ClientPost
    {
        [Key]
        public int clientPostId { get; set; }
        public string clientPostTitle { get; set; }
        public string clientPostdescription { get; set; }
        public string clientOwner { get; set; }
        public DateTime clientePostDate { get; set; }

    }
}
