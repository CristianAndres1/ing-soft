﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoloMastersWeb.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<SoloMastersWeb.Models.ClientPost> avisoClientes { get; set; }
        public DbSet<SoloMastersWeb.Models.MasterPost> avisoMaestros { get; set; }

    }
}
