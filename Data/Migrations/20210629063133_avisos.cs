﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SoloMastersWeb.Data.Migrations
{
    public partial class avisos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "avisoClientes",
                columns: table => new
                {
                    clientPostId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    clientPostTitle = table.Column<string>(nullable: true),
                    clientPostdescription = table.Column<string>(nullable: true),
                    clientOwner = table.Column<string>(nullable: true),
                    clientePostDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_avisoClientes", x => x.clientPostId);
                });

            migrationBuilder.CreateTable(
                name: "avisoMaestros",
                columns: table => new
                {
                    masterPostId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    masterPostTitle = table.Column<string>(nullable: true),
                    masterPostDescription = table.Column<string>(nullable: true),
                    masterOwner = table.Column<string>(nullable: true),
                    masterPostDate = table.Column<DateTime>(nullable: false),
                    imgUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_avisoMaestros", x => x.masterPostId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "avisoClientes");

            migrationBuilder.DropTable(
                name: "avisoMaestros");
        }
    }
}
