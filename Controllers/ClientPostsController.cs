﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SoloMastersWeb.Data;
using SoloMastersWeb.Models;

namespace SoloMastersWeb.Controllers
{
    public class ClientPostsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClientPostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Authorize]
        //Vista Maestros
        public async Task<IActionResult> MasterView()
        {
            return View(await _context.avisoClientes.ToListAsync());
        }

        [Authorize]
        public async Task<IActionResult> ClientOwnerPosts(String userFilter)//filtro de avisos de usuario
        {
            return View("IndexOwner", await _context.avisoClientes.Where(b => b.clientOwner.Contains(userFilter)).ToListAsync());
        }

        [Authorize]
        public async Task<IActionResult> BuscadorAviso(String PalabraBuscada) //buscador
        {
            return View("MasterView", await _context.avisoClientes.Where(b => b.clientPostTitle.Contains(PalabraBuscada)).ToListAsync());
           
        }



        [Authorize]
        // GET: ClientPosts (all)
        public async Task<IActionResult> Index()
        {
            return View(await _context.avisoClientes.ToListAsync());
        }

        [Authorize]
        // GET: ClientPosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientPost = await _context.avisoClientes
                .FirstOrDefaultAsync(m => m.clientPostId == id);
            if (clientPost == null)
            {
                return NotFound();
            }

            return View(clientPost);
        }

        [Authorize]
        //MasterView Details
        public async Task<IActionResult> MasterDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientPost = await _context.avisoClientes
                .FirstOrDefaultAsync(m => m.clientPostId == id);
            if (clientPost == null)
            {
                return NotFound();
            }

            return View(clientPost);
        }

        [Authorize]
        // GET: ClientPosts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClientPosts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("clientPostId,clientPostTitle,clientPostdescription,clientOwner,clientePostDate")] ClientPost clientPost)
        {
            if (ModelState.IsValid)
            {
                _context.Add(clientPost);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(clientPost);
        }

        [Authorize]
        // GET: ClientPosts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientPost = await _context.avisoClientes.FindAsync(id);
            if (clientPost == null)
            {
                return NotFound();
            }
            return View(clientPost);
        }

        // POST: ClientPosts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("clientPostId,clientPostTitle,clientPostdescription,clientOwner,clientePostDate")] ClientPost clientPost)
        {
            if (id != clientPost.clientPostId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(clientPost);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientPostExists(clientPost.clientPostId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(clientPost);
        }

        [Authorize]
        // GET: ClientPosts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientPost = await _context.avisoClientes
                .FirstOrDefaultAsync(m => m.clientPostId == id);
            if (clientPost == null)
            {
                return NotFound();
            }

            return View(clientPost);
        }

        // POST: ClientPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var clientPost = await _context.avisoClientes.FindAsync(id);
            _context.avisoClientes.Remove(clientPost);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClientPostExists(int id)
        {
            return _context.avisoClientes.Any(e => e.clientPostId == id);
        }
    }
}
