﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SoloMastersWeb.Data;
using SoloMastersWeb.Models;

namespace SoloMastersWeb.Controllers
{
    public class MasterPostsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MasterPostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Authorize]
        //Vista clientes
        public async Task<IActionResult> ClientView()
        {
            return View(await _context.avisoMaestros.ToListAsync());
        }

        [Authorize]
        public async Task<IActionResult> MasterOwnerPost(String userFilter)//filtro de servicios de maestro
        {
            return View("IndexMasterOwner", await _context.avisoMaestros.Where(b => b.masterOwner.Contains(userFilter)).ToListAsync());
        }

        [Authorize]
        public async Task<IActionResult> BuscadorServicio(String PalabraBuscada) //buscador de servicios
        {
            return View("ClientView", await _context.avisoMaestros.Where(b => b.masterPostTitle.Contains(PalabraBuscada)).ToListAsync());

        }

        [Authorize]
        // GET: MasterPosts
        public async Task<IActionResult> Index()
        {
            return View(await _context.avisoMaestros.ToListAsync());
        }

        [Authorize]
        // GET: MasterPosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var masterPost = await _context.avisoMaestros
                .FirstOrDefaultAsync(m => m.masterPostId == id);
            if (masterPost == null)
            {
                return NotFound();
            }

            return View(masterPost);
        }

        [Authorize]
        // ClientDetails
        public async Task<IActionResult> ClientDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var masterPost = await _context.avisoMaestros
                .FirstOrDefaultAsync(m => m.masterPostId == id);
            if (masterPost == null)
            {
                return NotFound();
            }

            return View(masterPost);
        }


        [Authorize]
        // GET: MasterPosts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MasterPosts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("masterPostId,masterPostTitle,masterPostDescription,masterOwner,masterPostDate,imgUrl")] MasterPost masterPost)
        {
            if (ModelState.IsValid)
            {
                _context.Add(masterPost);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(masterPost);
        }

        [Authorize]
        // GET: MasterPosts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var masterPost = await _context.avisoMaestros.FindAsync(id);
            if (masterPost == null)
            {
                return NotFound();
            }
            return View(masterPost);
        }

        // POST: MasterPosts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("masterPostId,masterPostTitle,masterPostDescription,masterOwner,masterPostDate,imgUrl")] MasterPost masterPost)
        {
            if (id != masterPost.masterPostId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(masterPost);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MasterPostExists(masterPost.masterPostId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(masterPost);
        }

        [Authorize]
        // GET: MasterPosts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var masterPost = await _context.avisoMaestros
                .FirstOrDefaultAsync(m => m.masterPostId == id);
            if (masterPost == null)
            {
                return NotFound();
            }

            return View(masterPost);
        }

        [Authorize]
        // POST: MasterPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var masterPost = await _context.avisoMaestros.FindAsync(id);
            _context.avisoMaestros.Remove(masterPost);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MasterPostExists(int id)
        {
            return _context.avisoMaestros.Any(e => e.masterPostId == id);
        }
    }
}
